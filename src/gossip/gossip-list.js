import "./gossip-list.css";
import Moment from "moment";
import "moment/locale/hu";

import React from "react";
import { Image } from "react-bootstrap";
import Linkify from "react-linkify";

export default class GossipList extends React.Component {
  formatDate(date) {
    if (!date) return "";

    const gossipMoment = Moment(date).locale("hu");

    if (gossipMoment.year() < new Date().getFullYear()) {
      return gossipMoment.format("- YYYY MMM DD, HH:mm");
    } else {
      return gossipMoment.format("- MMM DD, HH:mm");
    }
  }

  render() {
    return (
      <div className="gossip-container">
        <ul className="gossip-text">
          {this.props.gossips.map((gossip) => (
            <li key={gossip.id}>
              <Linkify
                componentDecorator={(decoratedHref, decoratedText, key) => (
                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    href={decoratedHref}
                    key={key}
                  >
                    {decoratedText}
                  </a>
                )}
              >
                {gossip.text}
              </Linkify>

              <span className="gossip-text-muted">
                {this.formatDate(gossip.createDate)}
              </span>

              {gossip.imageUrl && (
                <>
                  <br />
                  <Image
                    className="gossip-gif"
                    width={250}
                    src={gossip.imageUrl}
                  />
                </>
              )}
            </li>
          ))}
        </ul>
      </div>
    );
  }
}
