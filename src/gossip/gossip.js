import "./gossip.css";
import axios from "axios";
import moment from "moment";
import React from "react";
import { Button, Form, Image, InputGroup } from "react-bootstrap";
import toast from "react-hot-toast";
import { CSSTransition } from "react-transition-group";
import { GIPHY_API_KEY, GOSSIP_URL } from "../constants";
import GossipList from "./gossip-list";
import "moment/locale/hu";
import { Grid } from "@giphy/react-components";
import { GiphyFetch } from "@giphy/js-fetch-api";

export default class Gossip extends React.Component {
  state = {
    gossips: [],
    announcements: [],
    loaded: false,
    errorMessage: "",
    gifText: "",
    gifSearch: false,
    gifUrl: "",
    selectedFile: null,
  };

  giphyFetch = new GiphyFetch(GIPHY_API_KEY);

  componentDidMount() {
    this.scheduleLoadGossips();
  }

  async scheduleLoadGossips() {
    try {
      await this.loadGossips();
      setInterval(async () => {
        await this.loadGossips();
        this.setState({ errorMessage: "" });
      }, 30000);
    } catch (error) {
      if (!error.response) {
        console.log("error", error);

        this.setState({
          errorMessage: "Hiba történt a pletykák betöltése közben!",
        });

        return;
      }

      if (error.response.status === 403) {
        this.setState({
          errorMessage: error?.response?.data?.message,
        });
      }
      toast.error(error?.response?.data?.message, {
        icon: "😤",
      });
    }
  }

  async loadGossips() {
    await axios.get(GOSSIP_URL).then((res) => {
      let gossipsResponse = res.data;

      const gossips = gossipsResponse.filter(
        (gossip) => moment(gossip.createDate).year() >= 2000
      );
      const announcements = gossipsResponse.filter(
        (gossip) => moment(gossip.createDate).year() < 2000
      );

      this.setState({ gossips, announcements, loaded: true });
    });
  }

  handleFileSelection = (event) => {
    this.setState({ selectedFile: event.target.files[0], gifUrl: "" });
    console.log(`Selected file:`, event.target.files[0]);
  };

  handleChange = (event) => {
    if (this.state.gifSearch) {
      this.setState({ gifText: event.target.value });
    } else {
      this.setState({ text: event.target.value });
    }
  };

  handleSubmit = async (event) => {
    event.preventDefault();

    console.log("state", this.state);

    if (!this.state.selectedFile && !this.state.gifUrl) {
      // validate gossip text
      if (!this.state?.text || this.state.text?.length < 3) {
        toast.error(
          "Ha csak pletykálkodsz, ajánlott 3 karakternél többet mondani",
          {
            icon: "😏",
          }
        );
        return;
      }
    }

    try {
      let imageUrl;

      if (this.state.selectedFile) {
        const formData = new FormData();
        formData.append("file", this.state.selectedFile);

        const fileResponse = await axios.post(`${GOSSIP_URL}/upload`, formData);
        console.log(fileResponse);

        imageUrl = fileResponse.data.secure_url;
      }

      // send gossip
      await axios.post(GOSSIP_URL, {
        text: this.state.text,
        imageUrl: imageUrl || this.state.gifUrl,
      });

      // reset
      this.setState({ text: "", gifUrl: "", selectedFile: "" });
      event.target.reset();

      this.setState({ loaded: false });
      this.loadGossips();
    } catch (error) {
      if (error.response?.status === 429) {
        toast.error("Túl gyakran nem illik pletykálni ¯\\_(ツ)_/¯", {
          icon: "😤",
        });
      } else if (error.response?.status === 417) {
        console.error(`Image moderation error!`, error);

        toast.error("A feltöltött kép nem felelt meg a tartalomszűrésnek!", {
          icon: "🤬",
        });
      } else {
        console.error(`error during submit!`, error);

        toast.error("Hiba történt! :(", {
          icon: "😱",
        });
      }
    }
  };

  onGifClick = (gif) => {
    const gifUrl = gif?.images?.fixed_width?.url;

    document.getElementById("new-gossip-ig").scrollIntoView();

    this.setState({
      gifSearch: false,
      gifUrl,
      gifText: "",
      imageUpload: false,
      imageUrl: "",
    });
  };

  onToggleGifSearch = () => {
    const newValue = !this.state.gifSearch;
    this.setState({
      gifSearch: newValue,
      imageUpload: !newValue && this.state.imageUpload,
    });
  };

  onToggleImageUpload = () => {
    const newValue = !this.state.imageUpload;
    this.setState({
      gifSearch: !newValue && this.state.gifSearch,
      imageUpload: newValue,
    });
  };

  fetchGifs = (offset) => {
    if (this.state.gifText) {
      return this.giphyFetch.search(this.state.gifText, { offset, limit: 10 });
    } else {
      return this.giphyFetch.trending({ offset, limit: 10 });
    }
  };

  renderInputForm = () => {
    if (this.state.gifSearch) {
      return (
        <Form.Control
          minLength={2}
          type="text"
          placeholder={"GIF keresés"}
          onChange={this.handleChange}
          aria-describedby="basic-addon"
        />
      );
    } else {
      return (
        <>
          <Form.Control
            type="text"
            placeholder={"Hallottál valami újat?"}
            onChange={this.handleChange}
            aria-describedby="basic-addon"
          />
          <Button variant="light" id="button-addon" type="submit">
            pletty
          </Button>
        </>
      );
    }
  };

  // On file select (from the pop up)
  onFileChange = (event) => {
    // Update the state
    this.setState({ selectedFile: event.target.files[0] });
  };

  renderNewGossipForm = () => (
    <>
      <Form onSubmit={this.handleSubmit}>
        <InputGroup
          size="md"
          className="col-md-6 offset-md-3"
          id="new-gossip-ig"
        >
          <Form.Control
            type="file"
            variant="light"
            name="file"
            accept="image/*"
            // key={this.state.selectedFile || null}
            onChange={this.handleFileSelection}
            disabled={this.state.gifUrl}
          />
        </InputGroup>

        <InputGroup
          size="md"
          className="mb-3 col-md-6 offset-md-3"
          id="new-gossip-ig"
        >
          {this.state.gifUrl && (
            <Button
              variant="danger"
              onClick={() => this.setState({ gifUrl: null })}
            >
              x
            </Button>
          )}
          {this.state.selectedFile && (
            <Button
              variant="danger"
              onClick={() => this.setState({ selectedFile: null })}
            >
              x
            </Button>
          )}

          <Button
            className={this.state.gifSearch ? "btn-success" : "btn-light"}
            onClick={this.onToggleGifSearch.bind(this)}
            disabled={this.state.gifUrl || this.state.selectedFile}
          >
            GIF
          </Button>
          {this.renderInputForm()}
        </InputGroup>
        {!this.state.gifSearch && (
          <Form.Text className="gossip-hint">
            A beküldött pletyik csak egy napig lesznek láthatóak, és anonim
            módon kerülnek beküldésre. Jól fontold meg mit mondasz, mert a
            beküldés gyakorisága korlátozva van! :)
          </Form.Text>
        )}

        {this.state.gifUrl && (
          <>
            <Image
              src={this.state.gifUrl}
              thumbnail={true}
              height={200}
              width={200}
              onClick={() => this.setState({ gifUrl: null })}
            />
          </>
        )}
      </Form>

      {this.state.gifSearch && (
        <div className="col-md-12 gif-grid">
          <Grid
            onGifClick={this.onGifClick}
            fetchGifs={this.fetchGifs}
            key={this.state.gifText}
            width={window.innerWidth - 30}
            columns={Math.floor(window.innerWidth / 200)}
            gutter={5}
            noLink={true}
          />
        </div>
      )}
    </>
  );

  formatAnnouncementDate(date) {
    if (!date) return "";

    const gossipMoment = moment(date).locale("hu");

    return gossipMoment.format("MMM DD, HH:mm");
  }

  render() {
    if (this.state.errorMessage) {
      return (
        <div className="error-center h-100">{this.state.errorMessage}</div>
      );
    } else {
      return (
        <div className="gossip-wrapper h-100">
          <div>
            <div className="gossip-new row">{this.renderNewGossipForm()}</div>
            <hr />
            <div className="row gossip-list-row">
              <CSSTransition
                unmountOnExit
                in={this.state.loaded || false}
                timeout={1000}
                classNames="gossip"
              >
                <GossipList gossips={this.state.gossips} />
              </CSSTransition>
            </div>
          </div>

          {this.state.announcements && this.state.announcements.length > 0 && (
            <pre className="announcement-pre">
              {this.state.announcements.map((gossip) => (
                <div>
                  {gossip.text} -{" "}
                  <span>{this.formatAnnouncementDate(gossip.createDate)}</span>
                </div>
              ))}
            </pre>
          )}
        </div>
      );
    }
  }
}
