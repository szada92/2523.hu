import './Birthdays.css';
import Image from 'react-bootstrap/Image'
import ligeti1 from './image/ligeti1.jpg'
import ligeti2 from './image/ligeti2.jpg'
import ligeti3 from './image/ligeti3.jpg'
import ligeti4 from './image/ligeti4.jpg'
import ligeti5 from './image/ligeti5.jpg'
import ligeti6 from './image/ligeti6.jpg'
import ligeti7 from './image/ligeti7.jpg'
import ligeti8 from './image/ligeti8.jpg'
import ligeti9 from './image/ligeti9.jpg'
import ligeti10 from './image/ligeti10.jpg'
import ligeti11 from './image/ligeti11.jpg'
import ligeti12 from './image/ligeti12.jpg'
import peti1 from './image/peti/1.png'
import peti2 from './image/peti/2.jpg'
import peti3 from './image/peti/3.jpg'
import peti4 from './image/peti/4.jpg'
import peti5 from './image/peti/5.jpg'
import peti6 from './image/peti/6.jpg'
import peti7 from './image/peti/7.jpg'
import peti8 from './image/peti/8.jpg'
import peti9 from './image/peti/9.png'

export function getBirthDayPicture() {
  const todayIso = new Date().toISOString();
  const todayDate = todayIso.substring(5, todayIso.indexOf('T'));
  
  switch (todayDate) {
    case '03-03':
      return {
        name: 'Ligeti',
        images: [
          ligeti1,
          ligeti2,
          ligeti3,
          ligeti4,
          ligeti5,
          ligeti6,
          ligeti7,
          ligeti8,
          ligeti9,
          ligeti10,
          ligeti11,
          ligeti12,
          peti3,
        ],
      }
      case '03-04':
        return {
          name: 'Peti',
          images: [
            ligeti11,
            peti1,
            peti2,
            peti3,
            peti4,
            peti5,
            peti6,
            peti7,
            peti8,
            peti9,
          ],
        }
      default:
      return null;
  }
}

function randomIntFromInterval(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

export function renderBirthday() {
  const birthDayPicture = getBirthDayPicture(); 
  const randomImageIndex = birthDayPicture === null ? 0 : randomIntFromInterval(0, birthDayPicture.images.length - 1);

  return (
          <div className='row'>
            <main className="col-md-6 mx-auto align-self-center">
              <h5 className="p-5 cover-text">Isten éltessen sokáig {birthDayPicture.name}!</h5>
            </main>
            <div className='col-md-6 maxHeightImage'>
              <a href={birthDayPicture.images[randomImageIndex]} target='_blank' rel="noreferrer">
                <Image className='maxHeightImage' alt={birthDayPicture.name} src={birthDayPicture.images[randomImageIndex]} fluid={true} />
              </a>
            </div>
          </div>
  );
}
